//hybrid inheritance
class College{
    public void methodCollege(){
        System.out.println("The method of the class college is Parent class.");
    }
}
//simple inheritance
class Department extends College{
    public void methodDepartment(){
        System.out.println("The method of the class Department inherits from College class.");
    }
}
//multi-level inheritance
class Classes extends Department{
    public void methodClasses(){
        System.out.println("The method of the class Classes inherits from Department class.");
    }
}
//hierarchical inheritance
class Children extends Department{
    public void methodStudent(){
        System.out.println("The method of the class Children inherits from Department class.");
    }
}
public class Whole {
    public static void main(String []args){
        Department dept = new Department();
        Classes cls = new Classes();
        Children st = new Children();
        dept.methodCollege();
        cls.methodCollege();
        st.methodCollege();
        dept.methodDepartment();
        cls.methodDepartment();
        st.methodDepartment();
        cls.methodClasses();
        st.methodStudent();
    }
}

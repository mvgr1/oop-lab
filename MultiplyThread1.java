import java.util.Scanner;
class Thread2 extends Thread {
    Table t;
    Thread2(Table t){
        this.t=t;
    }
    public void run() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the table to display : ");
        int n = sc.nextInt();
        t.table(n);
    }
}
class Thread1 extends Thread {
    Table t;
    Thread1(Table t){
        this.t=t;
    }
    public void run() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the table to display : ");
        int n = sc.nextInt();
        t.table(n);
    }
}
class Table{
    synchronized void table(int n) {
        for(int i=1;i<=12;i++) {
            System.out.println(n + "*" + i + "=" + (n*i));
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
            e.printStackTrace();
            }
        }
    }
}
class MultiplyThread1 {
    public static void main(String[] args) {
        Table obj=new Table();
        Thread1 t1 = new Thread1(obj);
        Thread2 t2 =new Thread2(obj);
        t1.start();
        t2.start();
    }
}
import java.io.*;
import java.text.AttributedCharacterIterator;
class Person implements Serializable {
    private String name;
    public int number=101;
    public Person(String name) {
        this.name = name; 
    }
    public String getName() {
        return name;
    }
    protected Object readResolve() throws ObjectStreamException {
        return new Person(name.toUpperCase());
    }
    public String toString(){
        return name;  
    }  
    public int hashCode(){
        return number;
    }
}

class Person1{
    protected String getName(){
        return "Alekya";
    }
}
class Main2 {
    public static void main(String[] args) throws Exception {
        Person1 name = new Person1();
        System.out.println("\ngetName() is used.");
        System.out.println("The name is : "+name.getName()+"\n");
        System.out.println("Boolean equals to check if two objects have same value or not.");
        Boolean b1=new Boolean(true);
        Boolean b2=new Boolean(false);
        if(b1.equals(b2)){
            System.out.println("Returns true if both the objects have same value.\n");
        }
        else{
            System.out.println("Returns false.\n");
        }
        Person person1 = new Person("John");
        System.out.println("HAsh code");
        System.out.println(person1.hashCode()+"\n");
        System.out.println("toString() method");
        System.out.println(person1+"\n");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(person1);
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()));
        Person person2 = (Person) ois.readObject();
        System.out.println("readResolve() is used.");
        System.out.println("Original name: " + person1.getName());
        System.out.println("Deserialized name: " + person2.getName()+"\n");
    }
}

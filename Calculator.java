import java.util.*;
class Calculator{
    public static void main(String[] args){
        int x;
        Scanner a = new Scanner(System.in);
        System.out.println("Enter num1 and num2 values :");
        int num1 = a.nextInt();
        int num2 = a.nextInt();
        System.out.println("Enter your choice of arithmetic operations: 1.Addition\n2.subtraction\n3.multiplication\n4.division\n5.modular division\n");
        int ch = a.nextInt();
        if(ch==1){
            x=num1+num2;
            System.out.println("Addition of two numbers : "+x);
        }
        else if(ch==2){
            x=num1-num2;
            System.out.println("Subtraction of two numbers : "+x);
        }
        else if(ch==3){
            x=num1*num2;
            System.out.println("Multiplication of two numbers : "+x);
        }
        else if(ch==4){
            x=num1/num2;
            System.out.println("Division of two numbers : "+x);
        }
        else if(ch==5){
            x=num1%num2;
            System.out.println("Remainder is : "+x);
        }
        else{
            System.out.println("Wrong choice");
        }
    }
}

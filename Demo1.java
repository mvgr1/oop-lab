class Demo{
    protected int a=101;
    protected String s="Jack";
    //private int b=102;
    //private String name="Rose";
    public int c=103;
    public String movie="Titanic";
}
public class Demo1 extends Demo{
    private int p=20;
    public int q=10;
    protected int r=40;
    public void display(){
        System.out.println("protected  int a = "+a+"\nString name = "+s);
        //System.out.println("Private int b = "+b+"\nString name = "+name);
        System.out.println("Public int c = "+c+"\nString name = "+movie);
        System.out.println("Private int p = "+p+"\nString name = "+s);
        System.out.println("Public int q = "+q+"\nString name = Alekya");
        System.out.println("Protected int r = "+r+"\nString movie = "+movie);
    }
    public static void main(String[] args) {
        Demo1 obj = new Demo1();
        obj.display();
    }
}

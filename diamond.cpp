#include<iostream>
using namespace std;
class Person{
    public:
    string name1 = "anyone";
    void displaymsg1(){
        cout<<"Person is called "<<endl;
    }
    
};
class Faculty : public Person{
    public:
    string name2 = "sir";
    void displaymsg2(){
        cout<<"Faculty called "<<name2<<" "<<name1<<endl;
    }
};  
class Student : public Person{
    public:
    string name3 = "student";
    void displaymsg3(){
        cout<<"Student called "<<name3<<" "<<name1<<endl;
    }
    
};
class Total : public Faculty,public Student{
    public:
    string name4 = "total";
    void displaymsg4(){
        cout<<"Total called "<<name2<<" "<<name3<<" "<<name4<<endl;
    }
    
};
int main(){
    Total obj;
    //obj.displaymsg1();
    obj.displaymsg2();
    obj.displaymsg3();
    obj.displaymsg4();
}

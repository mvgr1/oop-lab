import myPack.*;
import java.util.Scanner;
public class Mypack {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter the n value :");
        int n = s.nextInt();
        System.out.print("The "+ n + "th fibonacci number is : ");
        System.out.println(fibonacci.fib(n));
        System.out.println("The fibonacci series upto "+ n +" terms :");
        fibonacci.fibSeries(n);
    }
}

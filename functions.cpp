#include<iostream>
using namespace std;
class Box{
   public:
        float a,b,c;
        void setvalues(float l,float w,float h){
            a=l;
            b=w;
            c=h;
        }
        void boxArea(float length,float width,float height)
        {
            cout<<"Area : "<<endl;
            cout<<2*(length*width+width*height+height*length)<<endl;
        }
        void boxVolume(float length,float width,float height);
        friend void displayBoxDimensions(Box &obj);
        inline void displayWelcomeMessage();      
        
};
void Box::boxVolume(float length,float width,float height){
   cout<<"Volume : "<<endl;
   cout<<length*width*height<<endl;


}
void displayBoxDimensions(Box &obj){
   cout <<"Length is : " <<obj.a <<endl;
   cout <<"Width is : " <<obj.b <<endl;
   cout <<"Height is : " <<obj.c <<endl;
}
void Box::displayWelcomeMessage(){
   cout<<"Inline function : Hii "<<endl;
}
int main(){
   float length,width,height;
   cout<<"enter the dimensions"<<endl;
   cin>>length>>width>>height;
   Box gift;
   gift.setvalues(length,width,height);
   gift.boxArea(length,width,height);
   gift.boxVolume(length,width,height);
   displayBoxDimensions(gift);
   gift.displayWelcomeMessage();
}

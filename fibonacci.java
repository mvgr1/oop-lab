package myPack;

public class fibonacci{
    public static int fib(int n){
        if (n<=1){
            return n;
        }
        else{
            return fib(n-1)+fib(n-2);
        }
    }

    public static void fibSeries(int n){
        int a=0;
        int b=1;
        int c;
        for (int i=1;i<=n;i++){
            System.out.println(a);
            c=a+b;
            a=b;
            b=c;
            }
        }
}

#include<iostream>
using namespace std;
class Class{
    public:
    virtual void student()=0;
    void cr(){
        cout<<"class representative."<<endl;
    }
};
class Teacher :public Class{
    public:
    void student(){
        cout<<"Listens to the teaching."<<endl;
    }
};
int main(){
    Teacher obj;
    obj.student();
    obj.cr();
}

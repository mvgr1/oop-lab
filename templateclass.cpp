#include<iostream>
using namespace std;
template <class Generic>
class Template{
    public :
    void multiply(Generic num1,Generic num2){
        cout <<"Product is : "<<num1*num2<<endl;
    }
    void multiply(Generic num1,Generic num2,Generic num3){
        cout<<"Product is : "<<num1*num2*num3<<endl;
    }
};
int main(){
    Template <float> obj;
    obj.multiply(2,3);
    obj.multiply(2,3.5,2.5);
}

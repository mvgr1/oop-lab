class Student{
    String flname;
    double sempercent;
    String clgname;
    int clgcode;
    Student(){
        clgname = "MVGR";
        clgcode = 33;
        System.out.println("The College name and College code is : ");
    }
    Student(String fullname,double sempercentage){
        this.flname = fullname;
        this.sempercent = sempercentage; 
    }
    protected void finalize(){
        System.out.println("Dead");
    }
    public static void main(String[] args){
        Student dconst = new Student();
        System.out.println(dconst.clgname);
        System.out.println(dconst.clgcode);
        Student pconst = new Student("Alekya",90);
        System.out.println("NAME : " + pconst.flname + "  PERCENT : " + pconst.sempercent);
        
        dconst=null;
        pconst=null;
        System.gc();
    }
}

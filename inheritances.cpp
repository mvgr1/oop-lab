#include<iostream>
using namespace std;

//hybrid inheritance
class College{
    public:
    void methodCollege(){
        cout<<"The method of the class college is Parent class."<<endl;
    }
};
//simple inheritance
class Department :public College{
    public:
    void methodDepartment(){
        cout<<"The method of the class Department inherits from College class."<<endl;
    }
};
//multi-level inheritance
class Classes :public Department{
    public:
    void methodClasses(){
        cout<<"The method of the class Classes inherits from Department class."<<endl;
    }
};
//hierarchical inheritance
class Children :public Department{
    public:
    void methodStudent(){
        cout<<"The method of the class Children inherits from Department class."<<endl;
    }
};
class Subjects {
    public:
    void methodSubjects(){
        cout<<"Programming subjects."<<endl;
    }
};
//multiple inheritance
class University : public College,public Subjects{
    public:
    void methodUniversity(){
        cout<<"This is a multiple inheritance."<<endl;
    }
};
int main(){
    University uni;
    Department dept;
    Classes cls;
    Children st;
    dept.methodCollege();
    cls.methodCollege();
    st.methodCollege();
    dept.methodDepartment();
    cls.methodDepartment();
    st.methodDepartment();
    cls.methodClasses();
    st.methodStudent();
    //uni.methodDepartment();
    //uni.methodClasses();
    uni.methodCollege();
    uni.methodSubjects();
    uni.methodUniversity();
}

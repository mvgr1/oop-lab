#include<iostream>
using namespace std;
class Template{
    public:
    template<typename myDataType1>
    void add(myDataType1 a,myDataType1 b){
        cout<<"The result is : "<<a+b<<endl;
    }
};
int main(){
    Template obj;
    obj.add<int>(5,7);
    obj.add<float>(2.3,3.3);
}

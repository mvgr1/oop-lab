class Mutable{
    public static void main(String[] args) {
        int a=11;
        System.out.println("The address of a in int is : "+System.identityHashCode(a));
        a=10;
        System.out.println("The address of a in int is : "+System.identityHashCode(a));

        String name="Alekya";
        System.out.println("The address of name in string is : "+System.identityHashCode(name));
        name="Surya";
        System.out.println("The address of name in string is : "+System.identityHashCode(name));

        Boolean c=false;
        System.out.println("The address of c in boolean is : "+System.identityHashCode(c));
        c=true;
        System.out.println("The address of c in boolean is : "+System.identityHashCode(c));

        StringBuffer sb=new StringBuffer("Hello ");  
        System.out.println("The address of sb in string buffer is : "+System.identityHashCode(sb));
        sb.append("Java");
        System.out.println("The address of sb in string buffer is : "+System.identityHashCode(sb));

    }
}

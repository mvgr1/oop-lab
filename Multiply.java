class Multi{
    void mul(int a,int b){
        System.out.println("The result is : "+a*b);
    }
    void display(){
        System.out.println("Multiplication is done!!");
    }
}
public class Multiply extends Multi{
    void mul(double x,double y,int z){
        System.out.println("The result is : "+x*y*z);
    }
    void display(){
        System.out.println("Overriding is done and multiplication is done !!");
    }
    public static void main(String[] args){
        Multiply obj = new Multiply();
        obj.mul(2,3);
        obj.mul(2.3,4.5,8);
        obj.display();
    }
}

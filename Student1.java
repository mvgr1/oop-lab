class Student1{
    String fName;
    int rNum;
    double semPercent;
    String cName;
    int cCode;
    Student1(String fullName,int rollNum,double semPercentage,String collegeName,int collegeCode){
        fName = fullName;
        rNum = rollNum;
        semPercent = semPercentage;
        cName = collegeName;
        cCode = collegeCode;
    }
    void displayMsg(){
        System.out.println(fName);
        System.out.println(rNum);
        System.out.println(semPercent);
        System.out.println(cName);
        System.out.println(cCode);
    }
    protected void finalize(){
        System.out.println("Dead");
    }
    public static void main(String[] args){
        Student1 Alekya = new Student1("Alekya",75,90,"MVGR",33);
        Alekya.displayMsg();
        Alekya=null;
        System.gc();
    }
}

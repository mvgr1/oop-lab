class Mul1{
    void mul(int a,int b){
        System.out.println("the result is : "+a*b);
    }
}
public class Mul extends Mul1{
    void mul(double x,double y,int z){
        System.out.println("The result is : "+x*y*z);
    }
    public static void main(String[] args){
        Mul obj = new Mul();
        obj.mul(2,3);
        obj.mul(2.3,4.5,8);
    }
}

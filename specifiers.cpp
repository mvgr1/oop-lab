#include<iostream>
using namespace std;
class Demo{
    protected: int a=101;
    protected :string s="Jack";
    private :int b=102;
    private :string name="Rose";
    public :int c=103;
    public :string movie="Titanic";
};
class Demo1 : public Demo{
    private :int p=20;
    public: int q=10;
    protected :int r=40;
    public:
    void display(){
        cout<<"protected  int a = "<<a<<"\nString name = "<<s<<endl;
        //cout<<"Private int b = "<<b<<"\nString name = "<<name<<endl;
        cout<<"Public int c = "<<c<<"\nString name = "<<movie<<endl;
        cout<<"Private int p = "<<p<<"\nString name = "<<s<<endl;
        cout<<"Public int q = "<<q<<"\nString name = Alekya"<<endl;
        cout<<"Protected int r = "<<r<<"\nString movie = "<<movie<<endl;
    }
};
int main(){
    Demo1 obj;
    obj.display();
}
